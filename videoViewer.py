import sys
from PySide import QtGui, QtCore
from matplotlib.backends.backend_qt4agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import MDSplus
import numpy as np


class MplCanvas(FigureCanvas):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)

        FigureCanvas.__init__(self, fig)
        self.setParent(parent)

        FigureCanvas.setSizePolicy(self,
                                   QtGui.QSizePolicy.Expanding,
                                   QtGui.QSizePolicy.Expanding)
        FigureCanvas.updateGeometry(self)
        # self.mpl_connect('motion_notify_event', self.mouse_moved)
        
    def mouse_moved(self, mouse_event):
        if mouse_event.inaxes:
            x, y = mouse_event.xdata, mouse_event.ydata
            FigureCanvas.mouse_moved_signal.emit(x,y)


class ApplicationWindow(QtGui.QMainWindow):
    def __init__(self):
        QtGui.QMainWindow.__init__(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setWindowTitle('Phantom Video Viewer')

        self.fileMenu = QtGui.QMenu('&File', self)
        self.fileMenu.addAction('&Quit', self.fileQuit,
                                 QtCore.Qt.CTRL + QtCore.Qt.Key_Q)
        self.menuBar().addMenu(self.fileMenu)
        self.mainWidget = QtGui.QWidget(self)
        
        p = self.palette()
        p.setColor(self.backgroundRole(), 'white')
        self.setPalette(p)
        
        vbox = QtGui.QVBoxLayout(self.mainWidget)
        
        # Matplotlib canvas
        self.dc = MplCanvas(self.mainWidget, width=5, height=4, dpi=100)
        vbox.addWidget(self.dc)
        
        # Shot entry box
        shotBox = QtGui.QHBoxLayout()
        self.shotEntry = QtGui.QLineEdit(self)
        self.shotEntry.setText('171025040')
        self.shotEntry.setInputMask('99 99 99 999')
        shotBox.addWidget(self.shotEntry)
        openButton = QtGui.QPushButton('Open shot')
        openButton.clicked.connect(self.openShot)
        shotBox.addWidget(openButton)
        prevShotButton = QtGui.QPushButton('Previous shot')
        prevShotButton.clicked.connect(self.prevShot)
        shotBox.addWidget(prevShotButton)
        nextShotButton = QtGui.QPushButton('Next shot')
        nextShotButton.clicked.connect(self.nextShot)
        shotBox.addWidget(nextShotButton)
        vbox.addLayout(shotBox)
        
        # Frame buttons
        frameBox = QtGui.QHBoxLayout()
        frameBox.addWidget(QtGui.QLabel('Range', self))
        self.beginEntry = QtGui.QLineEdit(self)
        self.beginEntry.setFixedWidth(40)
        frameBox.addWidget(self.beginEntry)
        self.endEntry = QtGui.QLineEdit(self)
        self.endEntry.setFixedWidth(40)
        frameBox.addWidget(self.endEntry)
        playButton = QtGui.QPushButton('Play')
        playButton.clicked.connect(self.play)
        frameBox.addWidget(playButton)
        prevFrameButton = QtGui.QPushButton('<')
        prevFrameButton.clicked.connect(self.prevFrame)
        frameBox.addWidget(prevFrameButton)
        nextFrameButton = QtGui.QPushButton('>')
        nextFrameButton.clicked.connect(self.nextFrame)
        frameBox.addWidget(nextFrameButton)
        self.subtractionCheckbox = QtGui.QCheckBox('Subtraction', self)
        frameBox.addWidget(self.subtractionCheckbox)       
        self.subtractionCheckbox.stateChanged.connect(self.updateFigureInPlace)
        self.autoscaleCheckbox = QtGui.QCheckBox('Autoscale', self)
        self.autoscaleCheckbox.stateChanged.connect(self.updateFigureInPlace)
        frameBox.addWidget(self.autoscaleCheckbox)       
        vbox.addLayout(frameBox)
         
        # Frame slider
        self.slider = QtGui.QSlider(QtCore.Qt.Horizontal, self)
        self.slider.setFocusPolicy(QtCore.Qt.NoFocus)
        self.slider.setGeometry(30, 500, 400, 30)
        self.slider.valueChanged[int].connect(self.changeFrame)
        vbox.addWidget(self.slider)       
        
        self.statusBar().showMessage('Enter a shot and press Open to begin. Up/down/left/right keys control frames too.')
        
        self.mainWidget.setFocus()
        self.setCentralWidget(self.mainWidget)
        
        self.autoScale = False
        self.framesDatabase = {}
        
    def keyPressEvent(self, e):
        if e.key() == QtCore.Qt.Key_Right:
            self.nextFrame()
        elif e.key() == QtCore.Qt.Key_Left:
            self.prevFrame()
        elif e.key() == QtCore.Qt.Key_Up:
            self.nextFrame(multiplier=100)
        elif e.key() == QtCore.Qt.Key_Down:
            self.prevFrame(multiplier=100)
        
    def mousePressEvent(self, event):
        try:
            focused_widget = QtGui.QApplication.focusWidget()
            focused_widget.clearFocus()
            QtGui.QMainWindow.mousePressEvent(self, event)
        except:
            pass
        
    def updateFigureInPlace(self):
        self.updateFigure(self.slider.value())
        
    def openShot(self):
        if not hasattr(self, 'c'):
            self.c = MDSplus.Connection('ssh://base@mds-trm-1.ipp-hgw.mpg.de')
        self.shot = int(self.shotEntry.text().replace(' ', ''))
        try:
            self.c.openTree('qoi', self.shot)
        except:
            self.statusBar().showMessage('Shot {} could not be opened'.format(self.shot))
            self.dc.axes.cla()
            return
        QtGui.qApp.processEvents()
        
        self.dc.axes.cla()
        self.computeInitialFigure()
        self.dc.axes.set_title(str(self.shot))
        self.updateFigure(0)
        QtGui.qApp.processEvents()
        
        self.numFrames = int(self.c.get('hardware.phantom.readings.num_frames'))
        self.frameRate = int(self.c.get('hardware.phantom.readings.frame_rate'))
        exposTime = float(self.c.get('hardware.phantom.readings.expos_time'))
        startTime = float(self.c.get('hardware.phantom.trigger'))-60
        endTime = startTime + self.numFrames*1./self.frameRate
        self.times = [startTime + i*1./self.frameRate for i in range(self.numFrames)]
        QtGui.qApp.processEvents()
        
        self.slider.setValue(0)
        self.slider.setMaximum(self.numFrames)
        self.statusBar().showMessage('Shot {}: {} frames, {} fps, {:.1f} us exposure, {:.3f} to {:.3f} s'
                                     .format(self.shot, self.numFrames, self.frameRate, exposTime, startTime, endTime))
        self.shotEntry.clearFocus()
        
    def computeInitialFigure(self):
        self.im = self.dc.axes.imshow(self.getFrame(0), origin='lower', cmap=plt.cm.gist_heat)
        if self.autoscaleCheckbox.isChecked():
            self.im.autoscale()

    def updateFigure(self, value):
        if hasattr(self, 'im'):
            if self.subtractionCheckbox.isChecked():
                self.im.set_data(self.subtractMin(value))
            else:
                self.im.set_data(self.getFrame(value))
        if self.autoscaleCheckbox.isChecked():
            self.im.autoscale()
        self.dc.draw()

    def changeFrame(self, value):
        try:
            self.statusBar().showMessage('Frame {}, {:.7f} s'.format(value, self.times[value]))
        except:
            pass
        self.updateFigure(value)

    def fileQuit(self):
        self.close()

    def closeEvent(self, ce):
        self.fileQuit()
        
    def nextShot(self):
        i = 0
        while i < 10:
            i += 1
            self.shot += 1
            self.shotEntry.setText(str(self.shot))
            try:
                self.c.openTree('qoi', self.shot)
                found = self.getFrame(0).any()
                self.openShot()
                break
            except:
                found = False
            QtGui.qApp.processEvents()    
                
        if not found:
            self.statusBar().showMessage('No shots found, try again')
            
    def prevShot(self):
        i = 0
        while i < 10:
            i += 1
            self.shot -= 1
            self.shotEntry.setText(str(self.shot))
            try:
                self.c.openTree('qoi', self.shot)
                found = self.getFrame(0).any()
                self.openShot()
                break
            except:
                found = False
            QtGui.qApp.processEvents()
                
        if not found:
            self.statusBar().showMessage('No shots found, try again')
        
    def prevFrame(self, multiplier=1):
        self.slider.setValue(self.slider.value()-1*multiplier)

    def nextFrame(self, multiplier=1):
        self.slider.setValue(self.slider.value()+1*multiplier)
        
    def play(self):
        begin = int(self.beginEntry.text())
        end = int(self.endEntry.text())
        anyFramesMissing = False
        for i in range(begin, end):
            key = str(self.shot) + str(i)
            if key not in self.framesDatabase.keys():
                anyFramesMissing = True
                break
        if anyFramesMissing: 
            self.statusBar().showMessage('Getting frame range')
            QtGui.qApp.processEvents()
            self.getFrameRange(begin, end)
        self.slider.setValue(begin)
        for i in range(begin, end):
            self.nextFrame()
            QtGui.qApp.processEvents()
        
    def subtractMin(self, index):
        interval = 10
        halfint = interval//2
        upperLim = self.numFrames - halfint
        if halfint <= index <= upperLim:
            boxcar = [self.getFrame(i) for i in range(index-halfint, index+halfint)]
        elif index < halfint:
            boxcar = [self.getFrame(i) for i in range(interval)]
        else:
            boxcar = [self.getFrame(i) for i in range(self.numFrames-interval, self.numFrames)]
        return self.getFrame(index) - np.min(np.array(boxcar), axis=0)
        
    def getFrame(self, index):
        key = str(self.shot) + str(index)
        try: 
            return self.framesDatabase[key]
        except:
            try:
                frame = np.rot90(self.c.get('getSegment(HARDWARE:PHANTOM:FRAMES,{})'.format(index)).data()[0])
                self.framesDatabase[key] = frame
                return frame
            except:
                self.statusBar().showMessage('No frames found for shot {}.'.format(self.shot))
                
    def getFrameRange(self, begin, end):
        # try
        startTime = self.c.get('getSegmentlimits(HARDWARE:PHANTOM:FRAMES,$)[0]', begin)
        endTime = startTime + (end-begin)*1./self.frameRate
        timeLims = [startTime, endTime]
        self.c.get('SETTIMECONTEXT($,$)', timeLims[0], timeLims[1])
        frames = np.array(self.c.get('hardware.phantom.frames'))
        for i, frame in enumerate(frames):
            key = str(self.shot) + str(begin+i)
            self.framesDatabase[key] = np.rot90(frame)


qApp = QtGui.QApplication(sys.argv)
qApp.aboutToQuit.connect(qApp.deleteLater)

aw = ApplicationWindow()

aw.show()
aw.raise_()
sys.exit(qApp.exec_())
