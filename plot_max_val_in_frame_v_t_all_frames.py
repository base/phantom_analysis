# -*- coding: utf-8 -*-
"""
Created on Wed May 31 17:30:19 2017
USAGE: python /home/IPP-HGW/jter/W7X/Phantom/python_codes/plot_max_val_in_frame_v_t.py shot_num start_fr_num end_fr_num

@author: jter
"""
import matplotlib.pyplot as plt
import numpy as np
import MDSplus as m
import sys
import os
import time
import gc


def plot_shot_max(shot_num=None, save=False):
    start = time.time()
    c.openTree('qoi', int(shot_num))
    num_frs = int(c.get('HARDWARE.PHANTOM.READINGS.NUM_FRAMES'))
    frame_rate = float(c.get('HARDWARE.PHANTOM.READINGS.FRAME_RATE'))
    start_time = float(c.get('HARDWARE.PHANTOM.TRIGGER'))
    start_time = start_time-60.
    f = np.array(c.get('hardware.phantom.frames'))
    print('Time taken to get frames: {}'.format(time.time()-start))

    max_data = np.max(f, (1,2))
    time_sub = np.array([start_time + i/frame_rate for i in range(num_frs)])
    plt.figure()
    plt.title(str(shot_num))
    plt.plot(time_sub, max_data)
    if save:
        plt.savefig('{}.png'.format(shot_num))
    else:
        plt.show()
    return (time_sub, max_data)


def save_buncha_shots():
    max_datas = {}
    shots = []
    shots.extend(range(170919001, 170919012))
    shots.extend(range(170920001, 170920066))
    shots.extend(range(170921001, 170921026))
    shots.extend(range(170926001, 170926076))
    shots.extend(range(170927001, 170927042))
    shots.extend(range(171004001, 171004051))
    shots.extend(range(171005001, 171005047))
    shots.extend(range(171010001, 171010031))
    shots.extend(range(171011001, 171011068))
    shots.extend(range(171012001, 171012056))
    shots.extend(range(171017001, 171017057))
    shots.extend(range(171018001, 171018046))
    shots.extend(range(171024001, 171024055))
    shots.extend(range(171025001, 171025062))
    shots.extend(range(171026001, 171026041))
    for shot in shots: 
        print('Doing shot {}'.format(shot))
        time, max_data = None, None
        try:
            time, max_data = plot_shot_max(shot_num=shot, save=True)
            max_datas[shot] = (time, max_data)
            np.save('max_datas', max_datas)
        except:
            pass
        gc.collect()


c = m.Connection('mds-data-1')
if __name__ == '__main__':
    plot_shot_max(shot_num=sys.argv[1])

