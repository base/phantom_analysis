# -*- coding: utf-8 -*-
"""
Created on October 04 17:30:19 2017

USAGE: python /home/IPP-HGW/jter/W7X/Phantom/python_codes/display_phantom_frames_mdsdata.py shot_num start_fr_num end_fr_num

@author: Sean Ballinger
"""
import matplotlib.pyplot as plt
from matplotlib import animation
import numpy as np
import MDSplus as m
import sys

def flip_horizontal(frames):
	return [f[::-1,:] for f in frames]

shot_num = int(sys.argv[1])
start_frame = int(sys.argv[2])
end_frame = int(sys.argv[3])
c=m.Connection('mds-data-1')
c.openTree('qoi',shot_num)
num_frs = int(c.get('HARDWARE.PHANTOM.READINGS.NUM_FRAMES'))
frame_rate = float(c.get('HARDWARE.PHANTOM.READINGS.FRAME_RATE'))
start_time = float(c.get('HARDWARE.PHANTOM.TRIGGER')) - 60
frames = np.array(c.get('hardware.phantom.frames'))
begin,end = start_frame,end_frame
time = [start_time+i/float(frame_rate) for i in range(start_frame, end_frame)]

fig, ax = plt.subplots()
im = plt.imshow(np.rot90(frames[start_frame]), origin='lower', cmap=plt.cm.gist_heat)
title = plt.title(str(time[0]))

def init():
	im.set_data(np.rot90(frames[start_frame]))
	return [im]

def animate(i):
	im.set_array(np.rot90(frames[i]))
	im.autoscale()
        title.set_text('%d %.6f' % (shot_num, time[i]))
	return [im]

plt.axis('off')
#plt.tight_layout(pad=0, rect=(0,0,1,1))

anim = animation.FuncAnimation(fig, animate, init_func=init, frames=end_frame-start_frame, interval=50, blit=False)
plt.show()
